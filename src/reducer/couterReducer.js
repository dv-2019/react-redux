import { Increment, Decrement } from '../action/CouterAction'
const initialCount = 0;
export const couterReducer = (state = initialCount, action) => {
    switch (action.type) {
        case Increment:
            return state + 1
        case Decrement:
            return state - 1
        default:
            return state
    }
}