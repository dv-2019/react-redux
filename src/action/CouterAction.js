export const Increment = 'Increment'
export const Decrement = 'Decrement'
export const increment = () => {
    const action = {
        type: Increment
    }
    return action;
}
export const decrement = () => {
    const action = {
        type: Decrement
    }
    return action;
}