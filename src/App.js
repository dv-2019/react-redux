import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Count} from './component/count'
import { CouterRedux } from './component/couterRedux';
function App() {
  return (
    <div className="App">
      <Count />
      <CouterRedux />
    </div>
  );
}

export default App;
