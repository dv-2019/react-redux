import { useSelector, useDispatch } from "react-redux"
import { decrement, increment } from '../action/CouterAction'
import React from 'react';
export const CouterRedux = () => {
    const counter = useSelector(state => state.couterReducer)
    const dispatch = useDispatch();

    return (
        <div>
            <div>
                Counts={counter}
            </div>
            <div>
                <button onClick={() => dispatch(decrement())}>-</button>
                <button onClick={() => dispatch(increment())}>+</button>
            </div>
        </div>
    )
}